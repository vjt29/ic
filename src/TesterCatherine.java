import java.io.IOException;
import java.util.ArrayList;
import java.util.DoubleSummaryStatistics;
import java.util.Random;

public class TesterCatherine {
	public static final int DVALUES = 3;
	public static final int DCLASSES = 10;
	public static final int FVALUES = 2;
	public static final int FCLASSES = 2;
	static int[][] subx;
	static int[] suby;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			int[][] dtestx = SerialReader.readDigits("resources/digitdata/testimages");
			int[] dtesty = SerialReader.readLabels("resources/digitdata/testlabels");

			// training data
			int[][] dtrainx = SerialReader.readDigits("resources/digitdata/trainingimages");
			int[] dtrainy = SerialReader.readLabels("resources/digitdata/traininglabels");

			// validation set
			int[][] dvalx = SerialReader.readDigits("resources/digitdata/validationimages");
			int[] dvaly = SerialReader.readLabels("resources/digitdata/validationlabels");
			
			int[][] ftestx = SerialReader.readFaces("resources/facedata/facedatatest");
			int[] ftesty = SerialReader.readLabels("resources/facedata/facedatatestlabels");
			
			// training data
			int[][] ftrainx = SerialReader.readFaces("resources/facedata/facedatatrain");
			int[] ftrainy = SerialReader.readLabels("resources/facedata/facedatatrainlabels");

			// validation set
			int[][] fvalx = SerialReader.readFaces("resources/facedata/facedatavalidation");
			int[] fvaly = SerialReader.readLabels("resources/facedata/facedatavalidationlabels");
			
			System.out.println("Digit Classfier");
			System.out.println();
			for(int i = 10; i <= 100; i+=10) {
				double[] sample = new double[10];
				double[] times = new double[10];
				for(int j = 0; j < 10; j++) {
					NaiveBayes fnb = new NaiveBayes(DCLASSES, DVALUES);
					fnb.setSmoothing(0.001);
					makeSubSet(i,dtrainx, dtrainy);
					long startTime = System.nanoTime();
					
					fnb.train(subx, suby);
					long endTime = System.nanoTime();

					double duration = (endTime - startTime); 
					times[j] = duration;
					//double fbestk = fnb.findBestk(fvalx, fvaly);
					//System.out.println( Best k: fbestk);
					double faccuracy = fnb.validate(dtestx, dtesty);
					sample[j] = faccuracy;
				}
				double mean = calcMean(sample);
				double avgtime = calcMean(times);
				double sd = calcStdDev(sample, mean);
				System.out.print(i + "%, " + avgtime/1000000 + "," );
				System.out.print(  mean + ",");
				System.out.print( sd + ",");
				System.out.println();
				
			}
			System.out.println();
			
			System.out.println("Face Classfier");
			System.out.println();
			for(int i = 10; i <= 100; i+=10) {
				double[] sample = new double[10];
				double[] times = new double[10];
				for(int j = 0; j < 10; j++) {
					NaiveBayes fnb = new NaiveBayes(FCLASSES, FVALUES);
					fnb.setSmoothing(0.001);
					makeSubSet(i,ftrainx, ftrainy);
					long startTime = System.nanoTime();
					
					
					fnb.train(subx, suby);
					long endTime = System.nanoTime();

					double duration = (endTime - startTime); 
					times[j] = duration;
					//double fbestk = fnb.findBestk(fvalx, fvaly);
					//System.out.println( Best k: fbestk);
					double faccuracy = fnb.validate(ftestx, ftesty);
					sample[j] = faccuracy;
				}
				double mean = calcMean(sample);
				double avgtime = calcMean(times);
				double sd = calcStdDev(sample, mean);
				System.out.print( i + "%, " + avgtime/1000000 + "," );
				System.out.print( mean + ",");
				System.out.print( sd + ",");
				System.out.println();
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		// initializing an NBC
		 
		 
		 
	}
	public static double calcMean(double sample[]) {
		double sum = 0.0;
		double sd = 0.0;
		
		for(double n:sample) {
			sum+=n;
		}
		
		return sum/10;
	}
	public static double calcStdDev(double sample[], double mean) {

		double sd = 0.0;
		
		
		for(double n:sample){
			sd += Math.pow(n - mean, 2);
		}
		sd = Math.sqrt(sd)/10;
		return sd;
	}
	public static void makeSubSet(int percent, int[][] x, int[] y) {
		if(percent == 100) {
			subx = x;
			suby = y;
			return;
		}
		int len = x.length * percent/ 100;
		int[][] subsetx = new int[len][x[0].length];
		int[] subsety = new int[len];
		ArrayList<Integer> al = new ArrayList<Integer>();
		for(int i = 0; i < subsetx.length; i++) {
			Random rand = new Random();
			int index = rand.nextInt(len);
			
			while(al.contains(index)) {
				index = rand.nextInt(len);
			}
			
			al.add(index);
			
			subsetx[i] = x[index];
			subsety[i] = y[index];
		}
		
		subx = subsetx;
		suby = subsety;
	}
	
	

}
