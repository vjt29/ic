import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ImageSerializer {
	public static final int DIGIT_WIDTH=28;
	public static final int DIGIT_HEIGHT=28;
	public static final int FACE_WIDTH=60;
	public static final int FACE_HEIGHT=70;
	
	public static digitData digitExtract(String file){
		String imgfile = "resources/digitdata/" + file + "images";
		String labelfile = "resources/digitdata/" + file + "labels";
		digitData data = new digitData();

		try{
			BufferedReader br = new BufferedReader(new FileReader(imgfile));
			String currLine = "";
			int lineCount = 0;
			String[] currimg = new String[DIGIT_WIDTH];
			//extract images from file
			while((currLine = br.readLine()) != null){
				if(lineCount >= DIGIT_HEIGHT){
					data.digits.add(currimg.clone());
					currimg = new String[DIGIT_WIDTH];
					lineCount = 0;
				}
				currimg[lineCount] = currLine;
				lineCount++;
			}
			if(lineCount > 0){
				data.digits.add(currimg.clone());
			}
			
			//get test data for digits
			br.close();
			br = new BufferedReader(new FileReader(labelfile));
			while((currLine = br.readLine()) != null){
				int num = Integer.parseInt(currLine);
				data.count[num]++;
			}
			for(int i = 0;i < 10;i++){
				data.Py[i] = (double)data.count[i]/(double)data.digits.size();
				System.out.println(data.Py[i]);
			}
			System.out.println(data.digits.size());
			
			//get Py by getting count for each digit, then normalizing
		}catch(Exception e){
			System.out.println("File not found.");
			return data;
		}
		return data;
	}

	
	public static faceData faceExtract(String file) throws IOException{
		String imgfile = "resources/facedata/" + file;
		String labelfile = "resources/facedata/" + file + "labels";
		faceData data = new faceData();
	
			BufferedReader br = new BufferedReader(new FileReader(imgfile));
			String currLine = "";
			int lineCount = 0;
			String[] currimg = new String[FACE_HEIGHT];
			//extract images from file
			while((currLine = br.readLine()) != null){
				if(lineCount >= FACE_HEIGHT){
					data.faces.add(currimg.clone());
					currimg = new String[FACE_HEIGHT];
					lineCount = 0;
				}
				currimg[lineCount] = currLine;
				lineCount++;
			}
			if(lineCount > 0){
				data.faces.add(currimg.clone());
			}
			
			//get test data for digits
			br.close();
			br = new BufferedReader(new FileReader(labelfile));
			while((currLine = br.readLine()) != null){
				int num = Integer.parseInt(currLine);
				data.count[num]++;
			}
			for(int i = 0;i < 2;i++){
				data.Py[i] = (double)data.count[i]/(double)data.faces.size();
				System.out.println(data.Py[i]);
			}
			System.out.println(data.faces.size());
			
			//get Py by getting count for each digit, then normalizing
		return data;
	}


	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		String test = "facedatatrain";
		
		faceData data = faceExtract(test);
		//can actually do bayes stuff now
	}

}
