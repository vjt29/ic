import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

public class SerialReader {

	public static final int DIGIT_WIDTH = 28;
	public static final int DIGIT_HEIGHT = 28;
	public static final int FACE_WIDTH = 60;
	public static final int FACE_HEIGHT = 70;

	/**
	 * reads a file and 'chunks' it into individual images, which are serialized
	 * 
	 * @param filepath
	 * @return an array
	 * @throws IOException
	 */
	public static int[][] readDigits(String filepath) throws IOException {

		// opening file and initializing return array
		BufferedReader br = new BufferedReader(new FileReader(filepath));
		int numImages = countLines(filepath) / DIGIT_HEIGHT;
		int digits[][] = new int[numImages][DIGIT_WIDTH * DIGIT_HEIGHT];

		// iterating over file and grouping and encoding pixels
		String line;
		int imgIndex;
		for (int img = 0; img < numImages; img++) {
			imgIndex = 0;
			for (int i = 0; i < DIGIT_HEIGHT; i++) {
				line = br.readLine();
				for (int j = 0; j < DIGIT_WIDTH; j++) {
					// assigning ints to different values for simplicity in
					// future steps
					switch (line.charAt(j)) {
					case ' ':
						digits[img][imgIndex] = 0;
						break;
					case '+':
						digits[img][imgIndex] = 1;
						break;
					case '#':
						digits[img][imgIndex] = 2;
						break;
					default:
						digits[img][imgIndex] = -1;
						break;
					};
					imgIndex++;
				}
			}
		}

		// avoiding memory leaks
		br.close();

		// return
		return digits;
	}

	/**
	 * reads a file and 'chunks' it into individual images, which are serialized
	 * 
	 * @param filepath
	 * @return an array
	 * @throws IOException
	 */
	public static int[][] readFaces(String filepath) throws IOException {

		// opening file and initializing return array
		BufferedReader br = new BufferedReader(new FileReader(filepath));
		int numImages = countLines(filepath) / FACE_HEIGHT;
		int faces[][] = new int[numImages][FACE_WIDTH * FACE_HEIGHT];

		// iterating over file and grouping and encoding pixels
		String line;
		int imgIndex;
		for (int img = 0; img < numImages; img++) {
			imgIndex = 0;
			for (int i = 0; i < FACE_HEIGHT; i++) {
				line = br.readLine();
				for (int j = 0; j < FACE_WIDTH; j++) {
					// assigning ints to different values for simplicity in
					// future steps
					switch (line.charAt(j)) {
					case ' ':
						faces[img][imgIndex] = 0;
						break;
					case '#':
						faces[img][imgIndex] = 1;
						break;
					default:
						faces[img][imgIndex] = -1;
						break;
					};
					imgIndex++;
				}
			}
		}

		// avoiding memory leaks
		br.close();

		// return
		return faces;
	}

	public static int[] readLabels(String filepath) throws IOException {

		// setup
		BufferedReader br = new BufferedReader(new FileReader(filepath));
		int numLables = countLines(filepath);
		int labels[] = new int[numLables];
		
		// iterating over file
		for(int i = 0; i < numLables; i++) {
			labels[i] = Integer.valueOf(br.readLine());
		}
		
		// return
		return labels;
	}

	/**
	 * Counts the number of lines in a file
	 * @param aFile
	 * @return the number of lines int he file
	 * @throws IOException
	 */
	public static int countLines(String filepath) throws IOException {
		LineNumberReader reader = null;
		try {
			reader = new LineNumberReader(new FileReader(filepath));
			while ((reader.readLine()) != null)
				;
			return reader.getLineNumber();
		} catch (Exception ex) {
			return -1;
		} finally {
			if (reader != null)
				reader.close();
		}
	}
}
