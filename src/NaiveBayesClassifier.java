
public class NaiveBayesClassifier {

	private PixelFrequency pf;

	public NaiveBayesClassifier(int[][] trainingdata, int[] labels, int values, int states) {
		pf = new PixelFrequency(trainingdata, labels, values, states);
	}
	
	public int classify(int[] observation) {
		// determining probability that the observation matches each state
		double[] probs = new double[pf.states];
		for (int state = 0; state < pf.states; state++) {
			probs[state] = prob(observation, state);
		}
		
		// returning the highest probability
		int maxindex = 0;
		double maxval = probs[maxindex];
		for (int i = 0; i < probs.length; i++) {
			if (probs[i] > maxval) {
				maxindex = i;
				maxval = probs[i];
			}
			//System.out.println(i + " " + probs[i]);
		}
		
		return maxindex;
	}

	/**
	 * Gives the probability that a particular observation correlates to a
	 * particular state
	 * 
	 * @param observations
	 * @param state
	 * @return the probability (given the training data) that the observation
	 *         matches the given state
	 */
	private double prob(int[] observations, int state) {
		// given state = y, yeilds log(P(y)) + SIGMA{log(P(f_i|y))}
		double prob = 0;

		// log(P(y))
		prob +=Math.log(pf.labelfreq[state]);

		// sum of P(fi|y)
		for (int i = 0; i < observations.length; i++) {
			//prob += Math.log(pf.datafreq[state][i][observations[i]]);
			prob += Math.log(approxFGivenY(state, i, observations[i]));
			
		}

		return prob;
	}
	
	private double approxFGivenY(int state, int obvindex, int value) {
		int total = 0;
		for (int i = 0; i < pf.pixelvalues; i++) {
			total += pf.datacounts[state][obvindex][i];
		}
		
		return pf.datacounts[state][obvindex][value] / (double) total;
		
	}

}
