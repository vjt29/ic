import java.util.ArrayList;
import java.util.Random;

public class NaiveBayes {
	double k;
	int classes;
	int values;
	int data [][][];
	double kgrid []={0.001, 0.01, 0.05, 0.1, 0.5, 1, 5, 10, 20, 50};;
	FeatureExtractor fe;
	public NaiveBayes(int classes, int values){
		this.classes=classes;
		this.values=values;
	}
	
	public void setSmoothing(double k){
		this.k=k;
		return;
	}
	
	public void train(int[][] trainx, int [] trainy) {

		
		fe = new FeatureExtractor(trainx,trainy,classes, values);
		data = fe.getXcount();      
	 

		
	}
	public double validate(int[][] valx, int[] valy) {
		int total = valy.length;
		int correct = 0;
	
		for (int j = 0; j < total; j++) {
			
			int estimate = classify(valx[j], classes);
			
			//System.out.println(estimate + " " + valy[j]);
			if(estimate == valy[j]) {

				correct++;
			};
		}
		
		double acc = correct/(double)total;
			
				
			
		
		return acc;
	};
	
	public double findBestk(int[][] valx, int[] valy) {
		int total = valy.length;
		int correct = 0;
		double bestk= 0.0;
		double maxacc = 0.0;
		for (int i = 0; i < kgrid.length;i++) {
			correct = 0;
			setSmoothing(kgrid[i]);
			for (int j = 0; j < total; j++) {
				
				int estimate = classify(valx[j], classes);
				if(estimate == valy[j]) {
					correct++;
				};
			}
			
			double tempacc = correct/(double)total;

			//System.out.print(tempacc);
			if (tempacc > maxacc) {
					maxacc = tempacc;
					bestk = kgrid[i];
				
			}	

			
		}
		setSmoothing(bestk);
		return bestk;
	};
	
	public int classify(int[] valx, int classes ) {
		
		double max = 1;
		int estimate = 0;
		
		for(int i = 0; i < classes; i++) {
			
				double temp = calcBayes(valx, i);
				//System.out.println(i + " " + temp);
				if (max == 1) {
					estimate = i;
					max=temp;
				}
				if (temp > max) {
					estimate = i;
					max = temp;
				}
			
		}
		//System.out.print(estimate + " ");
		return estimate;
	}
	
	public double calcBayes(int[] x, int y) {

		int ycount = fe.getYcount()[y];
		int ytotal = fe.getYtotal();
		
		
		double py =  ycount /(double) ytotal;
		//P(y) aka the postier probability of the given class given its frequency in the training set
		
		
		double pxy = 0.0;
		
		for (int i = 0; i < x.length; i++) {	
				//System.out.println(y + " " + i + " " + x[i]);
				//System.out.println(fe.getXcount()[y][i][x[i]] + " " + fe.getXtotal()[i][x[i]]);
				pxy += Math.log((fe.getXcount()[y][i][x[i]]+k)/((double) fe.getYcount()[y])+k);
							
		}
		double bayes = pxy+ Math.log(py);
		return bayes;
	}
}
