
public class PixelFrequency {

	// data frequencies
	public int datacounts[][][]; //[state][pixel][pixelvalue]
	public double labelfreq[];
	public int labelcounts[];
	
	// other metadata about the observations
	public int states;
	public int pixelvalues;

	/**
	 * Calculates the probability of each state ocurring for each pixel
	 * 
	 * @param samples
	 *            array of serialized images
	 * @param labels
	 *            array of associated labels
	 * @param states
	 *            number of classification values for the sample
	 */
	public PixelFrequency(int[][] samples, int[] labels, int pixelvalues, int states) {
		// initialization
		datacounts = new int[states][samples[0].length][pixelvalues];
		labelfreq = new double[states];
		labelcounts = new int[states];
		this.pixelvalues = pixelvalues;
		this.states = states;
		
		// setting frequency of each label in the dataset
		labelcounts = labecount(labels, states);
		for (int i = 0; i < states; i++) {
			labelfreq[i] = labelcounts[i] / (double) samples.length;
		}

		// frequencies of different pixel values
		// seeding values
		for (int i = 0; i < samples.length; i++) {
			for (int j = 0; j < samples[i].length; j++) {
				datacounts[labels[i]][j][samples[i][j]]++;
			}
		}

	}

	/**
	 * Counts the number of times each label occurs in the dataset
	 * @param labels
	 * @param states
	 * @return
	 */
	public static int[] labecount(int[] labels, int states) {
		int result[] = new int[states];
		for (int i = 0; i < labels.length; i++) {
			result[labels[i]]++;
		}

		return result;
	}
}
