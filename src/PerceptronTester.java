import java.io.IOException;

public class PerceptronTester {

	public static final int DIGIT_PIXEL_VALUES = 3;
	public static final int DIGIT_STATES = 10;
	public static final int FACE_PIXEL_VALUES = 2;
	public static final int FACE_STATES = 2;

	
	public static void testFaces() throws IOException {
		int[][] faces_training = SerialReader.readFaces("resources/facedata/facedatatrain");
		int[] flabels_training = SerialReader.readLabels("resources/facedata/facedatatrainlabels");
		int[][] faces_validate = SerialReader.readFaces("resources/facedata/facedatavalidation");
		int[] flabes_validate = SerialReader.readLabels("resources/facedata/facedatavalidationlabels");
		int[][] faces_test = SerialReader.readFaces("resources/facedata/facedatatest");
		int[] flabes_test = SerialReader.readLabels("resources/facedata/facedatatestlabels");
		
		
		int trainamountfaces = (int)(faces_training.length *1);
		
			Perceptron percp = new Perceptron(60, 70, 2);
			long startTime = System.nanoTime();
			percp.weight(faces_training,flabels_training,trainamountfaces,100);
			long endTime = System.nanoTime();
			System.out.println("Faces correctness percent: " + percp.test(faces_test, flabes_test));
	}


	public static void randTestFaces() throws IOException {
		int[][] faces_training = SerialReader.readFaces("resources/facedata/facedatatrain");
		int[] flabels_training = SerialReader.readLabels("resources/facedata/facedatatrainlabels");
		int[][] faces_validate = SerialReader.readFaces("resources/facedata/facedatavalidation");
		int[] flabes_validate = SerialReader.readLabels("resources/facedata/facedatavalidationlabels");
		int[][] faces_test = SerialReader.readFaces("resources/facedata/facedatatest");
		int[] flabes_test = SerialReader.readLabels("resources/facedata/facedatatestlabels");
		
		
		int trainamountfaces = (int)(faces_training.length *1);
		System.out.println(trainamountfaces);
		double[] acc = new double[10];
		double[] time = new double[10];
		
		for(int i = 0;i < 10;i++){
			Perceptron percp = new Perceptron(60, 70, 2);
			long startTime = System.nanoTime();
			percp.randweight(faces_training,flabels_training,trainamountfaces,100);
			long endTime = System.nanoTime();
			acc[i] = percp.test(faces_test, flabes_test);
			
			time[i] = (((double)(endTime - startTime))/1000000);
		}
		
		System.out.println("Acc:");
		meanStd(acc);
		System.out.println("Time:");
		meanStd(time);
	}


	public static void testDigits() throws IOException {
		int[][] digits_training = SerialReader.readDigits("resources/digitdata/trainingimages");
		int[] dlabels_training = SerialReader.readLabels("resources/digitdata/traininglabels");
		
		int[][] digits_validate = SerialReader.readDigits("resources/digitdata/validationimages");
		int[] dlabels_validate = SerialReader.readLabels("resources/digitdata/validationlabels");
		
		int[][] digits_test = SerialReader.readDigits("resources/digitdata/testimages");
		int[] dlabels_test = SerialReader.readLabels("resources/digitdata/testlabels");
		
		
		int trainamountdigits = (int)Math.ceil((digits_training.length * 1));
		System.out.println(trainamountdigits);
			Perceptron percp = new Perceptron(28, 28, 10);
			long startTime = System.nanoTime(); 
			percp.weight(digits_training, dlabels_training,trainamountdigits, 200);
			long endTime = System.nanoTime();
			System.out.println("Faces correctness percent: " + percp.test(digits_test, dlabels_test));
	}


	public static void randTestDigits() throws IOException {
		int[][] digits_training = SerialReader.readDigits("resources/digitdata/trainingimages");
		int[] dlabels_training = SerialReader.readLabels("resources/digitdata/traininglabels");
		
		int[][] digits_validate = SerialReader.readDigits("resources/digitdata/validationimages");
		int[] dlabels_validate = SerialReader.readLabels("resources/digitdata/validationlabels");
		
		int[][] digits_test = SerialReader.readDigits("resources/digitdata/testimages");
		int[] dlabels_test = SerialReader.readLabels("resources/digitdata/testlabels");
		
		
		int trainamountdigits = (int)Math.ceil((digits_training.length * 1));
		System.out.println(trainamountdigits);
		double[] acc = new double[10];
		double[] time = new double[10];
		
		for(int i = 0;i < 10;i++){
			Perceptron percp = new Perceptron(28, 28, 10);
			long startTime = System.nanoTime(); 
			percp.randweight(digits_training, dlabels_training,trainamountdigits, 100);
			long endTime = System.nanoTime();
			acc[i] = percp.test(digits_test, dlabels_test);
			
			time[i] = (((double)(endTime - startTime))/1000000);
		}
		System.out.println("Acc:");
		meanStd(acc);
		System.out.println("Time:");
		meanStd(time);
	}


	public static void main(String[] args) throws IOException{
			
			testFaces();			
			
			testDigits();
			
	}


	public static void meanStd(double[] acc) {
		double mean = 0;
		for(int i = 0;i < 10;i++){
			mean += acc[i];
		}
		mean /= acc.length;
		System.out.println("Mean: " + mean);
		double std = 0; 
		for(int i = 0;i < 10;i++){
			std += Math.pow(Math.abs(acc[i] - mean),2);
		}
		std /= acc.length;
		std = Math.sqrt(std);
		System.out.println("Std: " + std);
	}
	
}
