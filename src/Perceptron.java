import java.io.IOException;

public class Perceptron {

	int[][] weights; //[i][j], i= data state, j = feature
	int data_width;
	int data_height;
	int data_states;

	public Perceptron(int data_width, int data_height, int data_states) {
		this.data_width = data_width; 
		this.data_height = data_height;
		this.data_states = data_states;
		this.weights = new int[data_states][data_height*data_width+1];
	}
	
	/**
	 * @param data the 2d integer data array
	 * @param datalabels the labels for the data
	 * @param datamax the max amount of data entries to use, if 0 or less than 0 defaults to data.length
	 * @param max_repetitions the max amount of repetitions to train for
	 */
	public void weight(int[][] data, int[] datalabels, int datamax, int max_repetitions) {

		if(datamax > data.length){
			datamax = data.length;
		}
		if(datamax <= 0){
			datamax = data.length;
		}
		for(int repetitions = 0;repetitions < max_repetitions;repetitions++){
			boolean accurate = true;
			for(int i = 0;i < datamax;i++){
				int fxy[] = new int[data_states];
				for(int j = 0;j < data_states;j++){
					fxy[j] += weights[j][0];
				}
				for(int j = 0;j < data_height*data_width; j++){
					for(int k = 0;k < data_states;k++){
						fxy[k] += data[i][j]*weights[k][j+1];	
					}
				}
				int state = -1;
				int currmax = Integer.MIN_VALUE;
				for(int j = 0;j < data_states;j++){ //find the calculated state
					if(fxy[j] > currmax){
						currmax = fxy[j];
						state = j;
					}
				}
				if(state < 0){
					System.out.println(currmax);
					System.out.println("error");
					return;
				}
				if(state == datalabels[i]){
					continue;
				}
				else{
					accurate = false;
						weights[datalabels[i]][0]++;
						weights[state][0]--;
						for(int k = 0; k < data[i].length;k++){
							weights[datalabels[i]][k+1] += data[i][k];
							weights[state][k+1] -= data[i][k];
						}
					
				}
				
			}
			if(accurate){
				System.out.println("right");
				System.out.println(repetitions);
				break;
			}
		}
	}
	
	/**
	 * @param data the 2d integer data array
	 * @param datalabels the labels for the data
	 * @param datamax the max amount of data entries to use, if 0 or less than 0 defaults to data.length
	 * @param max_repetitions the max amount of repetitions to train for
	 */
	public void randweight(int[][] data, int[] datalabels, int datamax, int max_repetitions) {
	
		if(datamax > data.length){
			datamax = data.length;
		}
		if(datamax <= 0){
			datamax = data.length;
		}
		for(int repetitions = 0;repetitions < max_repetitions;repetitions++){
			boolean accurate = true;
			boolean[] used = new boolean[data.length];
			for(int i = 0;i < datamax;i++){
				int pickeddata = (int) (Math.ceil(Math.random()*(data.length-1)));
				while(used[pickeddata]){
					pickeddata = (int) (Math.ceil(Math.random()*data.length));
				}
				int fxy[] = new int[data_states];
				for(int j = 0;j < data_states;j++){
					fxy[j] += weights[j][0];
				}
				for(int j = 0;j < data_height*data_width; j++){
					for(int k = 0;k < data_states;k++){
						fxy[k] += data[pickeddata][j]*weights[k][j+1];	
					}
				}
				int state = -1;
				int currmax = Integer.MIN_VALUE;
				for(int j = 0;j < data_states;j++){ //find the calculated state
					if(fxy[j] > currmax){
						currmax = fxy[j];
						state = j;
					}
				}
				if(state < 0){
					System.out.println(currmax);
					System.out.println("error");
					return;
				}
				if(state == datalabels[pickeddata]){
					continue;
				}
				else{
					accurate = false;
						weights[datalabels[pickeddata]][0]++;
						weights[state][0]--;
						for(int k = 0; k < data[pickeddata].length;k++){
							weights[datalabels[pickeddata]][k+1] += data[pickeddata][k];
							weights[state][k+1] -= data[pickeddata][k];
						}
					
				}
				
			}
			if(accurate){
				System.out.println("right");
				System.out.println(repetitions);
				break;
			}
		}
	}

	public double test(int[][] data, int[] datalabels){
		int right = 0;
		int wrong = 0;
		for(int i = 0;i < data.length;i++){
			int fxy[] = new int[data_states];
			for(int j = 0;j < data_states;j++){
				fxy[j] += weights[j][0];
			}
			for(int j = 0;j < data_height*data_width; j++){
				for(int k = 0;k < data_states;k++){
					fxy[k] += data[i][j]*weights[k][j+1];	
				}
			}
			int state = -1;
			int currmax = Integer.MIN_VALUE;
			for(int j = 0;j < data_states;j++){ //find the calculated state
				if(fxy[j] >= currmax){
					state = j;
					currmax = fxy[j];
				}
			}
			if(state < 0){
				System.out.println("error");
				return 0;
			}
			if(state == datalabels[i]){
				right++;
			}
			else{
				wrong++;
			}
		}
		System.out.println("right: " + right + ", wrong: " + wrong);
		return ((double)right/((double)right + (double)wrong))*100;
	}

	public void printWeights(){
		for(int i = 0;i < weights.length;i++){
			System.out.print(i + ": ");
			for(int j = 0;j < weights[0].length;j++){
				System.out.print(weights[i][j] + ", ");
			}
			System.out.println();
		}
	}

}
