import java.io.IOException;

public class TesterKyle {

	public static final int DIGIT_PIXEL_VALUES = 3;
	public static final int DIGIT_STATES = 10;
	public static final int FACE_PIXEL_VALUES = 2;
	public static final int FACE_STATES = 2;

	public static void main(String[] args) throws IOException {
		/* Reading in and serializing digit data */
		// test data
		int[][] digits_test = SerialReader.readDigits("resources/digitdata/testimages");
		int[] dlabels_test = SerialReader.readLabels("resources/digitdata/testlabels");

		// training data
		int[][] digits_training = SerialReader.readDigits("resources/digitdata/trainingimages");
		int[] dlabels_training = SerialReader.readLabels("resources/digitdata/traininglabels");

		// validation set
		int[][] digits_validation = SerialReader.readDigits("resources/digitdata/validationimages");
		int[] dlabels_validation = SerialReader.readLabels("resources/digitdata/validationlabels");

		// initializing an NBC
		NaiveBayesClassifier nbcD = new NaiveBayesClassifier(
				digits_training, dlabels_training, DIGIT_PIXEL_VALUES, DIGIT_STATES);

		int correct = 0;
		for (int i = 0; i < digits_validation.length; i++) {
			if (nbcD.classify(digits_validation[i]) == dlabels_validation[i]) {
				correct++;
			}
		}

		System.out.println(correct + " " + digits_validation.length);

		/* Reading in and serializing face data */
		// test data
		int[][] faces_test = SerialReader.readFaces("resources/facedata/facedatatest");
		int[] flabels_test = SerialReader.readLabels("resources/facedata/facedatatestlabels");

		// training data
		int[][] faces_training = SerialReader.readFaces("resources/facedata/facedatatrain");
		int[] flabels_training = SerialReader.readLabels("resources/facedata/facedatatrainlabels");

		// validation set
		int[][] faces_validation = SerialReader.readFaces("resources/facedata/facedatavalidation");
		int[] flabels_validation = SerialReader.readLabels("resources/facedata/facedatavalidationlabels");


		// initializing an NBC
		NaiveBayesClassifier nbcF = new NaiveBayesClassifier(
				faces_training, flabels_training, FACE_PIXEL_VALUES, FACE_STATES);

		int fcorrect = 0;
		for (int i = 0; i < faces_validation.length; i++) {
			if (nbcF.classify(faces_validation[i]) == flabels_validation[i]) {
				fcorrect++;
			}
		}
		
		System.out.println(fcorrect + " " + faces_validation.length);

	}

}
