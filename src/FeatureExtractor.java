
public class FeatureExtractor {
	private int xcount[][][];//the number of classes times the number of features times potential values
	private int ycount[];
	private int ytotal;
	private int xtotal[][];
	
	
	public FeatureExtractor(int[][] x, int [] y, int classes,int values) {
		
		xcount = new int[classes][x[0].length][values];
		xtotal = new int[x[0].length][values];
		countLabels(classes, y);
		countFeatures(x, y);
	
	}
	
	public void countLabels(int classes, int[] y){
		ycount = new int[classes];
		for (int i=0; i < y.length;i++) {
			ycount[y[i]]++;
		};
		setYcount(ycount);
		setYtotal(y.length);
	}
	
	public void countFeatures(int x[][], int[] y) {
		
		for (int i = 0;i < y.length;i++) {
		//iterate through classes 0 - class number
			for(int j = 0; j < x[i].length;j++) {
			//iterate through training samples 
				//System.out.println(x[i][j]);
				xcount[y[i]][j][x[i][j]]++;
				xtotal[j][x[i][j]]++;
			}
		}
		
		return;
	}

	public int[] getYcount() {
		return ycount;
	}

	public void setYcount(int ycount[]) {
		this.ycount = ycount;
	}



	public int[][][] getXcount() {
		return xcount;
	}

	public void setXcount(int xcount[][][]) {
		this.xcount = xcount;
	}
	public int getYtotal() {
		return ytotal;
	}
	public void setYtotal(int ytotal) {
		this.ytotal = ytotal;
	}
	public int[][] getXtotal() {
		return xtotal;
	}
	public void setXtotal(int xtotal[][]) {
		this.xtotal = xtotal;
	}
	

}
